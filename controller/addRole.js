let addRole = function (message, config, send, sendRich, args) {
    let name = args[0],
        color = args[1]
    if (name && color) {
        guild = message.channel.guild
        if (!guild.roles.find(role => role.name.toLowerCase() === (config.prefix + name.toLowerCase()))) {
            guild.createRole({
                    name: config.prefix + name,
                    color: "" + color,
                })
                .then(function () {
                    console.log(message.content)
                    send("I created role " + name)
                })
                .catch(console.error)
        }
    }else{
        send("The name or color is missing")
    }
}

module.exports = addRole;