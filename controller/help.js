let help = (message, config, send, sendRich, args) => {
    let cmds = [
        "- `"+config.name+" add-role <name> <color>`\n  Create missing roles",
        "- `"+config.name+" add-techno <name>`\n  Assign to you the technology",
        "- `"+config.name+" list-techno`\n List available technology to affect ;)"
    ]
    sendRich("The available commands are :",cmds.join("\n\n"))
}

module.exports = help;