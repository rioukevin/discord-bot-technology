let addTechno = (message, config, send, sendRich, args) => {
    let name = args[0]
    if (name) {
        guild = message.channel.guild
        let Role = guild.roles.find(role => (role.name.toLowerCase() === (config.prefix + name.toLowerCase())))
        if (Role) {
            message.member.addRole(Role)
            send(`${name} is now a part of you`)
        } else {
            send(`${name} is not an available technology`)
        }
    }else{
        send("I think the name is missing")
    }
}

module.exports = addTechno;