const Discord = require("discord.js");
const config = require("./config");
const Koa = require("koa");

const app = new Koa();
const port = process.env.PORT || 8080;

let commands = [];
commands[config.name] = [];

//Import controllers
commands[config.name]["add-role"] = require("./controller/addRole");
commands[config.name]["add-techno"] = require("./controller/addTechno");
commands[config.name]["list-techno"] = require("./controller/listTechno");
commands[config.name]["help"] = require("./controller/help");

const client = new Discord.Client();

client.on("ready", () => {
  console.log("I am ready!");
});

client.on("message", (message) => {
  let send = (text) => {
    message.channel.send(text);
  };
  let sendRich = (title, text, color = config.color) => {
    let embed = new Discord.RichEmbed();
    embed.setTitle(title);
    embed.setColor(color);
    embed.setDescription(text);
    send(embed);
  };
  let params = message.content.split(" ");
  if (
    message.channel.name.localeCompare(config.channelName) == 0 &&
    params[0].toLowerCase().localeCompare(config.name.toLowerCase()) == 0
  ) {
    let args = [];
    let cmd = undefined;
    if (params[1] && commands[config.name][params[1]]) {
      args = params.slice(2, params.length);
      cmd = commands[config.name][params[1]];
      cmd(message, config, send, sendRich, args);
    } else {
      send("I don't understand sorry :(");
    }
  }
});

client.login(config.token);

app.use(async (ctx) => {
  ctx.body = "BrestJS Discord Technologies Bot";
});

app.listen(port);
